//
//  main.cpp
//  LengthExtensionAttack
//
//  Created by iKing on 20.11.17.
//  Copyright © 2017 iKing corp. All rights reserved.
//

#include <iostream>
#include <openssl/sha.h>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Easy.hpp>
#include "json.hpp"

using json_t = nlohmann::json;

bool makeRequest(std::string message, std::string mac) {
	std::replace(message.begin(), message.end(), ' ', '+');
	
	std::string url = "http://ec2-35-159-11-170.eu-central-1.compute.amazonaws.com/mac/trythis?message=" + message + "&mac=" + mac;
	
	std::list<std::string> header;
	header.push_back("Content-Type: application/json");
	
	curlpp::Cleanup clean;
	curlpp::Easy r;
	r.setOpt(new curlpp::options::Url(url));
	r.setOpt(new curlpp::options::HttpHeader(header));
	r.setOpt(new curlpp::options::PostFields(""));
	r.setOpt(new curlpp::options::PostFieldSize(0));
	
	std::ostringstream response;
	r.setOpt(new curlpp::options::WriteStream(&response));
	
	r.perform();
	
	json_t json = json_t::parse(response.str());
	
	if (json.find("correct") != json.end()) {
		bool correct = json.at("correct").get<bool>();
		return correct;
	}
	
	return false;
}

int main(int argc, const char * argv[]) {
	
	const std::string initialMessage = "Guys who understand that using Hash function as Mac is one very bad practice: Thai Duong; Juliano Rizzo; Flickr (the hard way);";
	const std::string append = "<your_name>";
	
	SHA_CTX context;
	SHA1_Init(&context);
	
	context.Nl = 1536;
	
	context.h0 = 0x094AB1E8;
	context.h1 = 0x05BF6A29;
	context.h2 = 0xEC29D7C4;
	context.h3 = 0xE1ECB87D;
	context.h4 = 0x524C8EC1;
	
	SHA1_Update(&context, append.c_str(), append.length());
	
	unsigned char hash[SHA_DIGEST_LENGTH];
	SHA1_Final(hash, &context);
	
	std::ostringstream macStream;
	macStream << std::uppercase << std::hex;
	for (int i = 0; i < SHA_DIGEST_LENGTH; i++) {
		macStream << (int) hash[i];
	}
	
	std::string mac = macStream.str();
	std::cout << mac << std::endl;
	
	size_t messageLength = initialMessage.length();
	
	size_t keyLength = 1;
	while (keyLength < 50) {
		std::ostringstream paddingStream;
		paddingStream << "%80";
		
		u_int64_t totalLength = (messageLength + keyLength) * 8;
		
		size_t paddingSize = (((440 - totalLength) % 512 + 512) % 512) / 8;
		for (size_t i = 0; i < paddingSize; ++i) {
			paddingStream << "%00";
		}
		
		u_int64_t reversedLength = __builtin_bswap64(totalLength);
		u_int8_t* p = (u_int8_t*) &reversedLength;
		for (size_t i = 0; i < 8; ++i) {
			paddingStream << "%" << std::setfill('0') << std::setw(2) << std::hex << (int) (*p++);
		}
		
		std::string padding = paddingStream.str();
		std::string message = initialMessage + padding + append;
		std::cout << keyLength << std::endl;
		if (makeRequest(message, mac)) {
			std::cout << "Key length is " << keyLength << std::endl;
			break;
		}
		
		keyLength++;
	}
	
	
	return 0;
}
